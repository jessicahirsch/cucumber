# Cucumber_Selenium

To run login test:

In root folder of repo, run
`CONFIG_FILE=single ./node_modules/cucumber/bin/cucumber.js features/login.feature`

To shorten this command ^:
1. $ `open ~/.bash_profile`
2. Add the following line to your bash

alias {{alias}}="./node_modules/cucumber/bin/cucumber.js"

3. Use whatever shortener variable you'd like in exchange
4. From terminal $ `source ~/.bash_profile`

5. Then run
`CONFIG_FILE=single {{alias}} features/login.feature`

All commands can be found in package.json.