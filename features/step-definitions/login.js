'use strict';

var loginPage = require('../pages/inturnLoginPage.js');
var {Given, Then, When} = require('cucumber');
var assert = require('cucumber-assert');
var chai = require('chai');  

var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style
var should = chai.should();  // Using Should style

var chrome = require('selenium-webdriver/chrome');
var path = require('chromedriver').path;
var webdriver = require('selenium-webdriver');

var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

module.exports = function() {

  this.Given(/^I go to "([^"]*)"$/, function(site) {
    return loginPage.go(site);
  });

  this.When(/^I type login as "([^"]*)"$/, function(task) {
    driver.findElement(webdriver.By.className('input iui-AuthContainer__field--input')).sendKeys(task)
  });  

  this.When(/^I type password as "([^"]*)"$/, function(task) {
    driver.findElement(webdriver.By.css('input[type="password"]')).sendKeys(task)
  });

  this.When(/^I click the login button$/, function() {
    driver.findElement(webdriver.By.css('button[type="filled"]')).click()
  });

  this.Then(/^I should see the avatar$/, function() {
    driver.findElement(webdriver.By.className("iui-InventorySection__create-offer-button"))
  });

};